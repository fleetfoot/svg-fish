# SVG Fish

I've always wanted to learn 3D animation, so I decided to turn a doodle of mine into an SVG animation. The fish are originally 3D objects in Blender, I then used Freestyle SVG Exporter to create animated SVG's. It required some tweaking of the SVG code and filling in colors in Inkscape but finally I got something I was happy with.
